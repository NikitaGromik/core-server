<?php

require_once('config.php');
require_once('utils.php');

class DatabaseManager {
	
	private $DatabaseReference;

	public function __construct($config) {
		if (!isset($config) || empty($config) || !is_array($config)) {
			onServerErrorDetected();
			die();
		}

		try {
            $this->DatabaseReference = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_name'], $config['db_user'], $config['db_pass']);
        } catch (PDOException $e) {
        	onServerErrorDetected();
            echo 'Error!: ' . $e->getMessage() . '<br/>';
            die();
        }

	}

	/**
	 * Проверяет наличие кода в БД, а также его актуальность.
	 * 
	 * Возвращает true, если такой промокод действителен.
	 */
	private function isCorrectCode($code) {
		$strQuery = 'SELECT `_ActivatedAt` FROM `Promocodes` WHERE `_Code` = :CodeValue LIMIT 1';

		$request = $this->getDB()->prepare($strQuery);

        $request->execute(array(
            'CodeValue' => $code,
        ));

        $row = $request->fetch(PDO::FETCH_NUM);
        if (!$row) {
        	// Если записи вообще не существует, то возвращаем ложь.
        	return false;
        }

        // Проверка, что код не был ранее активирован.
        // Под нулевой позицией находится поле _ActivatedAt.
        if (!is_null($row[0])) {
        	return false;
        }

        return true;
	}

	/**
	 * Функция активации промокода.
	 * 
	 * Возвращает true в случае успешной активации.
	 */
	public function activate($code) {
		if (!$this->isCorrectCode($code)) {
			echo $this->toJson([
				'status' => 'Такого промокода не существует, либо он был активирован ранее.',
			]);
			return false;
		}

		$strQuery = 'UPDATE `Promocodes` SET `_ActivatedAt` = :CurrentDateTimeValue WHERE `_Code` = :CodeValue';

		$request = $this->getDB()->prepare($strQuery);

		$status = $request->execute([
			'CurrentDateTimeValue' => date("Y-m-d H:i:s"),
			'CodeValue' => $code,
		]);

		if ($status === false || $request->rowCount() <= 0) {
			// Ни одна строка не была обновлена.
			onServerErrorDetected();
			return false;
		}

		echo $this->toJson($this->getCodeInfo($code));
	}

	/**
	 * Выдает информацию о промокоде.
	 * 
	 * Возвращает массив значений.
	 * 
	 * Выбрасывает 404 ошибку, если такого промокода нет в БД.
	 */
	private function getCodeInfo($code) {
		$strQuery = 'SELECT `_Code`, `_Type`, `_Value` FROM `Promocodes` WHERE `_Code` = :CodeValue LIMIT 1;';

		$request = $this->getDB()->prepare($strQuery);

		$request->execute([
			'CodeValue' => $code,
		]);

		$row = $request->fetch(PDO::FETCH_ASSOC);

		if (!$row) {
			onNotFound();
			return;
		}

		return $row;
	}

	public function toJson($value) {
		return json_encode($value, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Getter DatabaseReference.
	 */
	public function getDB() {
		return $this->DatabaseReference;
	}

}