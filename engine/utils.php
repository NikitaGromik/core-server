<?php

/**
 * Возвращает 500 код ошибки.
 */
function onServerErrorDetected() {
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);	
}

/**
 * Устанавливает 404 ошибку.
 */
function onNotFound() {
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
}

function getEpisode($episodeName = '') {
	if (empty($episodeName)) {
		return '';
	}	

	return file_get_contents('./episodes/' . $episodeName . '.json');
}