<?php

header('Content-Type: application/json');
header('Accept: application/json');

require_once('./engine/dbManager.php');

require_once('./engine/utils.php');

$DatabaseManager = new DatabaseManager($AppConfiguration);

function get($s = '') {
    return isset($_POST[$s]) ? $_POST[$s] : '';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$_POST = json_decode(file_get_contents('php://input'), true);

	if (isset($_POST['action'])) {
		switch ($_POST['action']) {
			case 'activate':
				$DatabaseManager->activate(get('code'));
				break;
			case 'episode':
				echo getEpisode(get('name'));
			default:
				//onServerErrorDetected();
				break;
		}
	} else {
		onServerErrorDetected();
	}
} else {
	onServerErrorDetected();
}